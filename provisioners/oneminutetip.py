"""OneMinuteTip provisioner/provider."""
import copy
import pathlib
import subprocess

from bs4 import BeautifulSoup as BS
from cki_lib.misc import safe_popen
from ruamel.yaml.scalarstring import PreservedScalarString

from plumbing.interface import ProvisionerCore
from plumbing.objects import ResourceGroup
from upt.logger import LOGGER
from upt.misc import fixup_or_delete_tasks_without_fetch


class OneMinuteTip(ProvisionerCore):
    """OneMinuteTip provisioner."""

    # pylint: disable=no-self-use,no-member,protected-access
    def __init__(self, dict_data=None):
        """Construct instances after deserialization."""
        super().__init__(dict_data=dict_data)

        self.wrapper_path = str(pathlib.Path(pathlib.Path(__file__).absolute().parents[0],
                                             '../plumbing', 'oneminutetip_wrap.py'))
        # Override wait times, this provisioner is fast and synchronous. In seconds.
        self.evaluate_wait_time = 1
        self.evaluate_confirm_time = 1

    def get_distro_requires(self, recipe):
        """Extract distro_name value from a distroRequires element."""
        distroreq = recipe.find('distroRequires')
        if not distroreq:
            raise RuntimeError('cannot find distroRequires element')

        distro_name = distroreq.find('distro_name')
        if not distro_name:
            raise RuntimeError('cannot find distro_name element')

        return distro_name['value']

    def provision(self, **kwargs):
        """Provision all hosts in all resource groups."""
        self.keycheck = kwargs.get('keycheck')

        preprovision_arg = kwargs.get('preprovision')
        preprovision_indexes = [int(x) for x in preprovision_arg.split(',')] if preprovision_arg \
            else []
        # Add pre-provisioned resource groups.
        self.rgs += [ResourceGroup(copy.deepcopy(self.rgs[idx].dict_data)) for idx in
                     preprovision_indexes]
        if not preprovision_indexes:
            LOGGER.debug('* No pre-provisioning enabled.')

        for resource_group in self.rgs:
            # We currently don't have a use for resource_id with 1minutetip, so just use an
            # increasing unique number.
            resource_group.resource_id = str(self.resource_id_monotonic.get())
            recipe_set = resource_group.recipeset
            soup = BS(recipe_set.restraint_xml, 'xml')
            recipes = soup.find_all('recipe')
            resource_group.ssh_opts = '-o GSSAPIAuthentication=no -i /usr/share/qa-tools/' \
                                      '1minutetip/1minutetip'

            assert len(recipes) == len(recipe_set.hosts), "Invalid data; must have 1 host per 1 " \
                                                          "recipe"

            for j, host in enumerate(recipe_set.hosts):
                host.recipe_id = self.host_recipe_id_monotonic.get()
                distro_name = self.get_distro_requires(BS(host.recipe_fill, 'xml'))
                host.misc['instance_id'], host.hostname = self.provision_host(distro_name)

                recipes[j]['id'] = host.recipe_id

            resource_group._provisioning_done = True
            fixup_or_delete_tasks_without_fetch(soup)
            recipe_set.restraint_xml = PreservedScalarString(soup.prettify())

    def provision_host(self, system):
        """Provision a single host."""
        stdout, stderr, retcode = safe_popen([self.wrapper_path, 'provision', system],
                                             stdout=subprocess.PIPE,
                                             stderr=subprocess.PIPE)
        if stderr:
            LOGGER.error(stderr)

        if retcode:
            raise RuntimeError('failed to provision host')

        return stdout.strip().split(':')

    def is_provisioned(self, _):
        """Check if resource group is finished provisioning."""
        return True

    def get_provisioning_state(self, _):
        """Get current state of provisioning."""
        return True, []

    def reprovision_aborted(self):
        """Provision a resource again, if provisioning failed."""

    def release_resources(self):
        """Release all instances."""
        for resource_group in self.rgs:
            for host in resource_group.hosts():
                if host.misc:
                    safe_popen([self.wrapper_path, 'cancel', host.misc['instance_id']])

    def heartbeat(self, resource_group, recipe_ids_dead):
        """Check if resource is OK (provisioned, not broken/aborted)."""

    def update_provisioning_request(self):
        """Ensure that request file has up-to-date info after provisioning.

        This method is to be called at the end, when we know we've successfully
        provisioned resources we need. This method will skip resources that
        failed provisioning.
        """

    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""
        return [rg.resource_id for rg in self.rgs]

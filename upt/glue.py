"""Gluecode that is run on provisioners to provide cli-advertised functions."""

from upt.misc import RET


class ProvisionerGlue:
    # pylint: disable=too-many-instance-attributes
    """Glue that uses provisioners to provide user functions."""

    def __init__(self, provisioner_data, **kwargs):
        """Create the object."""
        self.provisioner_data = provisioner_data
        self.provisioners = provisioner_data.provisioners

        self.kwargs = kwargs
        self.request_file = kwargs['request_file']
        self.rc_data = kwargs['rc_data']

    def do_wait(self):
        # pylint: disable=too-many-arguments
        """Use provisioner method to wait for resources to be ready."""
        retcodes = []

        for provisioner in self.provisioners:
            retcodes.append(provisioner.wait(provisioner.get_resource_ids(), **self.kwargs))

            # Update provisioning request file
            provisioner.update_provisioning_request()

        # Serialize it
        self.provisioner_data.serialize2file(self.kwargs['request_file'])

        return RET.merge_retcodes(retcodes)

    def run_provisioners(self):
        # pylint: disable=too-many-arguments
        """Provision all resources."""
        for provisioner in self.provisioners:
            provisioner.provision(**self.kwargs)

        # Update provisioning request, so resource_ids are reflected
        self.provisioner_data.serialize2file(self.request_file)

        retcode = self.do_wait()

        # Update provisioning request, so recipe_ids are reflected after waiting!
        self.provisioner_data.serialize2file(self.request_file)

        return retcode

"""Misc utility methods."""
from enum import Enum
import pathlib

from bs4 import BeautifulSoup as BS
from cki.beaker_tools.process_logs import File
from cki_lib import misc

from upt import const
from upt.logger import LOGGER

SESSION = misc.get_session(__name__)


class SanitizeLink:
    """Class to download and sanitize url contents."""

    def __init__(self, link, output_path, **kwargs):
        """Create the object."""
        self.link = link
        self.output_path = output_path
        self.parameters = kwargs

    @staticmethod
    def download_url(url):
        """Download. Handles retries."""
        LOGGER.debug('Downloading %s', url)

        response = SESSION.get(url)
        return response.content

    def save(self):
        """Save sanitized contents to a file."""
        content = self.download_url(self.link)
        self.output_path.mkdir(parents=True, exist_ok=True)
        path2file = self.output_path.joinpath(pathlib.Path(self.link).name)
        path2file.write_bytes(content)

        ffile = File(path2file, self)
        ffile.sanitize()

        return path2file


class Monotonic:
    # pylint: disable=too-few-public-methods
    """Get monotonic number."""

    def __init__(self, start_i=1):
        """Create an object; the counter is initialized to 1 by default."""
        self.__i = start_i

    def get(self):
        """Increase counter and return it."""
        retval = self.__i
        self.__i += 1
        return retval


class OutputDirCounter:
    # pylint: disable=too-few-public-methods
    """A counter that helps ensure test results are put in the right place."""

    number: Monotonic = Monotonic()

    def __init__(self):
        """Create an object."""
        # Number <1..N> coresponding to the number of hosts in the testplan.
        self.counter_number = OutputDirCounter.number.get()

    @property
    def path(self):
        """Get the prefix of a path where to store test results."""
        return f'results_{self.counter_number:04d}'


class RET(Enum):
    """Wraps all program return codes."""

    # Provisioning done successfully.
    PROVISIONING_PASSED = 0

    # For whatever reason, we've failed to provision a resource.
    # Maybe we've run out of time to finish provisioning. Or maybe there were
    # too many failures while doing that.
    PROVISIONING_FAILED = 1

    @classmethod
    def merge_retcodes(cls, retcodes):
        """Combine multiple provisioner retcodes into one.

        The most severe retcode from the list will be returned, such that
        a passing retcode is only returned if all retcodes are passing.
        """
        return max(retcodes, key=lambda x: x.value, default=cls.PROVISIONING_PASSED)


def recipe_not_provisioned(recipe):
    """Return status if recipe aborted and cannot run tasks."""
    status = recipe.get('status')
    return status if status in ('Aborted', 'Cancelled') else False


def recipe_installing_or_waiting(recipe):
    """Return True if recipe is installing."""
    return recipe.get('status') in ('Installing', 'Waiting')


def reservesys_task_problem(tasks):
    """Return cause if any task in tasks list aborted, cancelled or failed."""
    for task in tasks:
        if task['status'] in ('Aborted', 'Cancelled'):
            return task['status']
        if task['result'] == 'Fail':
            return task['result']

    return False


def fixup_or_delete_tasks_without_fetch(soup):
    """Delete <task /> elements that do not have <fetch /> in <params />."""
    for task in soup.find_all('task'):
        if not task.find_all('fetch'):
            if task['name'] == '/distribution/command':
                url = const.BKR_CORE_TASKS_URL.format(task='command')

                task.append(BS(f'<fetch url="{url}" />', 'xml').find('fetch'))

            else:
                # Delete task
                LOGGER.warning('%s removed from XML, no fetch element!', task["name"])
                task.decompose()


def compute_tasks_duration(tasks):
    """Add-up KILLTIMEOVERRIDE of all tasks in a list."""
    tasks_duration = 0
    for task in tasks:
        for param in task.findAll('param'):
            if param['name'] == 'KILLTIMEOVERRIDE':
                value = param['value']
                try:
                    tasks_duration += int(value)
                except ValueError:
                    LOGGER.debug('task duration was invalid: %s', value)

                break

    return tasks_duration


def compute_recipe_duration(recipe):
    """Add-up KILLTIMEOVERRIDE of all tasks in a recipe."""
    return compute_tasks_duration(recipe.findAll('task'))


def get_whiteboard(text, i, is_preprovisioned):
    """Modify whiteboard so we can tell separate jobs apart."""
    if i is not None:
        text = text.replace('</whiteboard>', f' #{i:02d}</whiteboard>')

    if is_preprovisioned:
        text = text.replace('</whiteboard>', ' PREPROVISIONED</whiteboard>')

    return text


def is_task_waived(task):
    """Check if a Beaker task XML has CKI_WAIVED param that is set to true."""
    try:
        params = task.find_all('param')
    except AttributeError:
        return False

    for param in params:
        try:
            if param['name'] == 'CKI_WAIVED' and str(param['value']).lower() \
                    == 'true':
                return True
        except KeyError:
            continue

    return False

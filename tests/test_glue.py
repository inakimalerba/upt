"""Test cases for glue module."""
import os
import unittest
from unittest import mock

from cki_lib.misc import tempfile_from_string

from plumbing.format import ProvisionData
from tests.const import ASSETS_DIR
from upt.glue import ProvisionerGlue
from upt.misc import RET


class TestGlue(unittest.TestCase):
    """Test cases for glue module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')
        self.prov_data = ProvisionData.deserialize_file(self.req_asset)

    def test_do_wait(self):
        """Ensure do_wait calls expected methods."""
        bkr = self.prov_data.get_provisioner('beaker')
        with mock.patch.object(bkr, 'wait', new=lambda *args, **kwargs: RET.PROVISIONING_PASSED):
            with mock.patch.object(bkr, 'update_provisioning_request', new=lambda: None):
                with tempfile_from_string(b'') as fname:
                    mock_rc = mock.Mock()
                    glue = ProvisionerGlue(self.prov_data, **{'request_file': fname,
                                                              'rc': 'whatever-path',
                                                              'rc_data': None})
                    glue.do_wait()

                    glue = ProvisionerGlue(self.prov_data, **{'request_file': fname,
                                                              'rc': 'whatever-path',
                                                              'rc_data': mock_rc})
                    self.assertEqual(glue.do_wait(), RET.PROVISIONING_PASSED)
                    glue = ProvisionerGlue(self.prov_data, **{'request_file': fname,
                                                              'rc': 'whatever-path',
                                                              'rc_data': mock_rc})
                    self.assertEqual(glue.do_wait(), RET.PROVISIONING_PASSED)

    @mock.patch('upt.glue.ProvisionerGlue.do_wait', return_value=RET.PROVISIONING_FAILED)
    @mock.patch('plumbing.format.ProvisionData.serialize2file')
    @mock.patch('provisioners.beaker.Beaker.provision')
    @mock.patch('provisioners.beaker.Beaker.update_provisioning_request')
    def test_run_provisioners(self, _, __, ___, ____):
        """Ensure run_provisioners works and calls expected methods."""
        mock_rc_data = mock.Mock()
        with tempfile_from_string(b'') as fname:
            glue = ProvisionerGlue(self.prov_data, **{'request_file': fname, 'rc': 'whatever-path',
                                                      'rc_data': mock_rc_data})
            self.assertEqual(RET.PROVISIONING_FAILED, glue.run_provisioners())

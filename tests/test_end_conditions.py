"""Test cases for end_conditions module."""
import time
import unittest
from unittest import mock

from plumbing.end_conditions import ProvEndConds


class FakeRg:
    """Fake resource group."""

    def __init__(self, erred_rset_ids, provisioning_done=False):
        self._erred_rset_ids = erred_rset_ids
        self._provisioning_done = provisioning_done


class TestEndConditions(unittest.TestCase):
    """Test cases for ProvEndConds class."""

    def test_abort_count_exceeded(self):
        # pylint: disable=no-self-use
        """Ensure we don't reprovision when we run out of attempts."""
        cond = ProvEndConds([FakeRg({0, 1, 2})], None, None)
        with mock.patch.object(cond, attribute='update') as prov_update:
            with mock.patch.object(cond, attribute='reprovision_aborted') as prov_reprovision:
                # Run .evaluate()
                cond.evaluate(False, max_aborted_count=3)
                # We need to update data each time
                prov_update.assert_called()
                # However, we've exceeded number of aborts len(_erred_rset_ids) == 3, so reprovision_aborted must not
                # be called.
                prov_reprovision.assert_not_called()

    def test_end_provisioning_success(self):
        """Ensure resource groups with _provisioning_done set to True evaluate to True (stop execution)."""
        cond = ProvEndConds([FakeRg({}, True)], None, None)
        with mock.patch.object(cond, attribute='update'):
            with mock.patch.object(cond, attribute='reprovision_aborted'):
                result = cond.evaluate(False, max_aborted_count=3, time_cap=False, clone_cap=False)
                self.assertTrue(result)

    def test_end_provisioning_fail(self):
        """Ensure resource groups with _provisioning_done set to False evaluate to False (keep waiting)."""
        cond = ProvEndConds([FakeRg({}, False)], None, None)
        with mock.patch.object(cond, attribute='update'):
            with mock.patch.object(cond, attribute='reprovision_aborted'):
                result = cond.evaluate(False, max_aborted_count=3, time_cap=False, clone_cap=False)
                self.assertFalse(result)

    @mock.patch('upt.logger.LOGGER.error')
    def test_timeout_eval_true(self, mock_logging):
        # pylint: disable=protected-access
        """Ensure timeout_eval works."""
        # We claim start_time was 61 seconds ago
        start_time = time.time() - 61

        # We test with timeout at 1 minute
        result = ProvEndConds._timeout_eval(start_time, 1)
        # Timeout occurred
        self.assertTrue(result)
        # Message was printed
        mock_logging.assert_called_with('* Provisioning canceled after 1 minutes.')

    def test_timeout_eval_false(self):
        # pylint: disable=protected-access
        """Ensure timeout_eval works."""
        # We claim start_time is now
        start_time = time.time()

        # We test with timeout at 600 minutes
        result = ProvEndConds._timeout_eval(start_time, 600)
        # Timeout shouldn't have occurred, 600 minutes is a long time to execute a couple of lines
        self.assertFalse(result)

    def test_timeout_timecap(self):
        """Ensure time_cap timeout works (1000s limit)."""
        cond = ProvEndConds([FakeRg({}, False)], None, None)
        result = cond.timeout(**{'time_cap': 1000, 'clone_cap': None})
        self.assertFalse(result)

    def test_timeout_timecap_clonecap(self):
        """Ensure time_cap + clonecap timeout works (1000s limit)."""
        cond = ProvEndConds([FakeRg({}, False)], None, None)
        result = cond.timeout(**{'time_cap': 1000, 'clone_cap': 1000})
        self.assertFalse(result)

    def test_timeout_timecap_clonecap_start_abort(self):
        """"Ensure time_cap + clonecap + abort timeout works (1000s limit)."""
        cond = ProvEndConds([FakeRg({}, False)], None, None)
        with mock.patch.object(cond, 'get_abort_count', lambda: 3):
            result = cond.timeout(**{'time_cap': 1000, 'clone_cap': 1000})
            self.assertFalse(result)

    def test_timeout_timecap_clonecap_start_no_abort(self):
        """Ensure time_cap timeout works (1000s limit)."""
        cond = ProvEndConds([FakeRg({}, False)], None, None)
        with mock.patch.object(cond, 'clonecap_start') as mock_cond:
            mock_cond.clonecap_start = time.time()

            result = cond.timeout(**{'time_cap': 1000, 'clone_cap': 1000})
            self.assertFalse(result)

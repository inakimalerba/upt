"""Test cases for provisioner interface module."""
import copy
import itertools
import os
import unittest
from unittest import mock

from plumbing.format import ProvisionData
from plumbing.objects import Host
from plumbing.objects import RecipeSet
from plumbing.objects import ResourceGroup
from provisioners.oneminutetip import ProvisionerCore
from tests.const import ASSETS_DIR
from upt.misc import RET


class TestProvisionerCore(unittest.TestCase):
    """Test cases for interface module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        self.bkr = prov_data.get_provisioner('beaker')

    def test_set_reservation_duration(self):
        """Ensure ProvisionerCore's set_reservation_duration returns False."""
        self.assertFalse(ProvisionerCore.set_reservation_duration(None))

    def test_find_objects_rgs(self):
        """Ensure find_objects works by getting ResourceGroups."""
        # return objects that are instances of ResourceGroup class
        result = self.bkr.find_objects(self.bkr.rgs, lambda x: x if isinstance(x, ResourceGroup) else None)
        self.assertEqual(self.bkr.rgs, result)

    def test_find_objects_recipests(self):
        """Ensure find_objects works by getting RecipeSets."""
        # return objects that are instances of RecipeSet class
        result = self.bkr.find_objects(self.bkr.rgs, lambda x: x if isinstance(x, RecipeSet) else None)
        self.assertEqual([self.bkr.rgs[0].recipeset], result)

    def test_find_objects_hosts(self):
        """Ensure find_objects works by getting Hosts."""
        # return objects that are instances of Host class
        result = self.bkr.find_objects(self.bkr.rgs, lambda x: x if isinstance(x, Host) else None)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts, result)

    def test_get_all_hosts(self):
        """Ensure get_all_hosts works."""
        result = self.bkr.get_all_hosts(self.bkr.rgs)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts, result)

    def test_find_host_object(self):
        """Ensure find_host_object works."""
        first_host = self.bkr.find_host_object(self.bkr.rgs, 123)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts[0], first_host)

        second_host = self.bkr.find_host_object(self.bkr.rgs, 456)
        self.assertEqual(self.bkr.rgs[0].recipeset.hosts[1], second_host)

    def test_all_recipes_finished(self):
        """Ensure all_recipes_finished works."""
        result = self.bkr.all_recipes_finished(self.bkr.rgs)
        self.assertFalse(result)

    def test_all_recipes_finished_true(self):
        # pylint: disable=protected-access
        """Ensure all_recipes_finished works by setting host as 'done processing'."""
        bkr = copy.deepcopy(self.bkr)
        for host in bkr.rgs[0].recipeset.hosts:
            host._done_processing = True

        result = bkr.all_recipes_finished(bkr.rgs)
        self.assertTrue(result)

    @mock.patch('plumbing.interface.ProvEndConds')
    @mock.patch('plumbing.interface.time.sleep', lambda x: None)
    @mock.patch('builtins.print', mock.Mock())
    def test_wait_(self, mock_cond):
        """Ensure wait is if-else complete."""
        def one_run(_):
            try:
                _ = one_run.done
                return True
            except AttributeError:
                one_run.done = True
                return False

        mock_cond.return_value.evaluate = one_run
        mock_cond.return_value.retcode = RET.PROVISIONING_FAILED
        mock_cond.return_value._timeout_eval.return_value = True

        with mock.patch.object(self.bkr, 'set_reservation_duration') as mock_set:
            with mock.patch.object(self.bkr, 'release_resources') as mock_release:
                mock_release.side_effect = itertools.chain([RuntimeError()],
                                                           itertools.cycle([None]))
                self.bkr.wait([rg.resource_id for rg in self.bkr.rgs])

                mock_set.assert_called()
                mock_release.assert_called()

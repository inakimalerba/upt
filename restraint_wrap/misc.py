"""Misc utility methods."""
import os
from urllib.parse import quote

from twisted.internet import error


def add_directory_suffix(output, instance_no):
    """Add numerical .done.XY suffix to directory."""
    dirname = f'{os.path.abspath(output)}.done.{instance_no:02d}'
    assert not os.path.isdir(dirname)
    return dirname


def attempt_reactor_stop(reactor):
    """If twisted reactor is running, try to stop it."""
    if reactor.running:
        try:
            reactor.callFromThread(reactor.stop)
        except error.ReactorNotRunning:
            pass


def attempt_heartbeat_stop(heartbeat_loop):
    """Attempt to stop heartbeat looping call."""
    try:
        heartbeat_loop.stop()
    except (AssertionError,  AttributeError):
        pass


def convert_path_to_link(path, is_file):
    """Get a quoted url to path, or just path if we're not running in pipeline."""
    prefix = os.environ.get('CI_JOB_URL', '')
    prefix += f'/artifacts/{"file" if is_file else "browse"}/artifacts/' if prefix else ''

    return prefix + quote(str(path)) if prefix else path

"""Provisioner interface and common methods."""

import time

from upt.logger import LOGGER
from upt.misc import RET

# pylint: disable=protected-access


class ProvEndConds:
    # pylint: disable=too-many-instance-attributes
    """Implements provisioning timeouts and checks whether it's finished."""

    def __init__(self, rgs, update, reprovision_aborted):
        """Create the object."""
        # Resource groups of provisioning request
        self.rgs = rgs
        # Method to check if resource is provisioned
        self.update = update
        # Method to provision a resource again, if provisioning failed
        self.reprovision_aborted = reprovision_aborted

        # Time when we've started provisioning
        self.start = time.time()
        # Optional; when first resource fails to provision, we set this and
        # start counting down a timeout.
        self.clonecap_start = None

        # initially, we assume no resources are provisioned; breaking execution
        # means we've failed to provision
        self.provisioned = [False]
        self.retcode = RET.PROVISIONING_FAILED

        # A list of methods to evaluate to see if we should end provisioning.
        self.end_conditions = [self.provisioning_success,
                               self.is_aborted_count_exceeded, self.timeout]

    def is_aborted_count_exceeded(self, **kwargs):
        """Check if too many resources "aborted" or "canceled"."""
        max_aborted_count = kwargs['max_aborted_count']

        return self.get_abort_count() >= max_aborted_count

    def get_abort_count(self):
        """Get count of hosts that "aborted" or "canceled"."""
        erred_count = 0
        for resource_group in self.rgs:
            erred_count += len(resource_group._erred_rset_ids)

        return erred_count

    @classmethod
    def _timeout_eval(cls, start_time, time_cap):
        """Evaluate if we've exceeeded time_cap minutes."""
        if int((time.time() - start_time) / 60.0) >= time_cap:
            msg = f'* Provisioning canceled after {time_cap} ' \
                  f'minutes.'
            LOGGER.error(msg)

            return True

        return False

    def timeout(self, **kwargs):
        """Check if any timeout occurred."""
        time_cap = kwargs['time_cap']
        clone_cap = kwargs['clone_cap']

        if time_cap and not clone_cap:
            return self._timeout_eval(self.start, time_cap)

        if time_cap and clone_cap:
            if self.clonecap_start is None:
                if self.get_abort_count():
                    # start clone_cap timer
                    self.clonecap_start = time.time()

                    return False
            else:
                return self._timeout_eval(self.clonecap_start, time_cap)

        return False

    def provisioning_success(self, **kwargs):
        # pylint: disable=unused-argument
        """Check if all resources are successfully provisioned.

        This method is evaluated first, so if all resources are successfully
        provisioned, then we will stop execution (break out of
        provisioning/timeout/max-abort check.
        """
        for resource_group in self.rgs:
            if not resource_group._provisioning_done:
                return False

        self.retcode = RET.PROVISIONING_PASSED

        return True

    def evaluate(self, force_recheck, **kwargs):
        """Check if we're done provisioning and get return code."""
        # Always refresh info about resources
        self.update(force_recheck=force_recheck)
        # Reprovision aborted recipes and mark them as such
        if not self.is_aborted_count_exceeded(**kwargs):
            self.reprovision_aborted()
        else:
            self.retcode = RET.PROVISIONING_FAILED
            return True

        for condition in self.end_conditions:
            # Check conditions until we find one that ends loop
            if condition(**kwargs):
                return True

        return False

"""Datastructure definition and validation for provisioning request."""

from typing import List

from ruamel.yaml.scalarstring import PreservedScalarString

from plumbing.serializer import DeserializerBase


class Host(DeserializerBase):
    """Datastructure describing a host to provision."""

    duration: int

    # host common, can't use a base class because of serialization
    hostname: str
    recipe_id: int
    recipe_fill: PreservedScalarString

    _done_processing: bool = False
    _panicked: bool = False
    _kernel_under_test_installed: bool = False

    _task_results: list = []
    _planned_tests: list = []
    _rerun_recipe_tasks_from_index: int = None

    misc: dict = {}
    # AWS Only
    _user_data_done: bool = False
    _instance: bool = None


class RecipeSet(DeserializerBase):
    """Datastructure describing a recipeset to provision in Beaker."""

    hosts: List[Host]
    restraint_xml: PreservedScalarString
    # Recipset id like 'RS:1'
    id: str = None

    _attempts_made: int = 0
    _tests_finished: bool = False
    _waiting_for_rerun: bool = False


class ResourceGroup(DeserializerBase):
    """A group of resources provisioned together."""

    resource_id: str
    recipeset: RecipeSet
    job: str
    _provisioning_done: bool = False
    _erred_rset_ids: set = set()
    _reprovisioned_rsets_ids: set = set()
    _provisioned_ok_rsets_ids: set = set()
    preprovisioned: bool = False
    # SSH options passed to restraint when making an ssh connection to hostnames in this rg.
    ssh_opts: str = ''

    def hosts(self):
        """Iterate through all hosts."""
        for host in self.recipeset.hosts:
            yield host

"""AWS launch template setup."""
import base64
from datetime import datetime
import json
import os

import boto3

from upt.logger import LOGGER

UPT_DOT_PREFIX = 'upt.default'
UPT_DEFAULT_KEYNAME = f"{UPT_DOT_PREFIX}.key"

LAUNCH_TEMPLATE_NAME = f'{UPT_DOT_PREFIX}.launch-template'

# https://alt.fedoraproject.org/cloud/ x86_64 us-east-1
FEDORA_AMI_ID = "ami-09f17ac4a76fd9ffe"
AWS_UPT_USER_DATA = b'''#!/bin/bash
curl -o /etc/yum.repos.d/beaker-harness.repo \
    https://beaker-project.org/yum/beaker-harness-Fedora.repo
dnf install -y restraint restraint-rhts make
# allow root ssh instead of just fedora
sed -i 's/.*ssh-rsa/ssh-rsa/' /root/.ssh/authorized_keys'''


class AWSSetup:
    # pylint: disable=no-member,too-many-instance-attributes
    """Setup AWS account for simple/default UPT use."""

    def __init__(self):
        """Create the object."""
        self.session = boto3.Session()
        self.ec2_resource = self.session.resource('ec2')
        self.ec2_client = self.session.client('ec2')

        self.vpc_tag = self.get_tag('vpc')
        self.vpc_filter = self.tag_value2filter(self.vpc_tag)

    @property
    def aws_config(self):
        """Get aws configuration for UPT."""
        return json.loads(os.environ['AWS_UPT_CONFIG'])

    @staticmethod
    def get_tag(resource):
        """Get a tag that marks a given a resource (e.g. 'vpc') as belonging to UPT."""
        return {"Key": "Name", "Value": f'{UPT_DOT_PREFIX}.{resource}'}

    @staticmethod
    def tag_value2filter(tag):
        """Create a filter for a specific tag."""
        return [{'Name': f'tag:{tag["Key"]}', 'Values': [tag['Value']]}]

    def create_default_vpc_with_tags(self, your_ip):
        """Create a default vpc and tag objects so they are easy to remove without affecting anything else."""
        # Create vpc and check response
        response = self.ec2_client.create_default_vpc()
        vpc = response.get('Vpc')

        assert vpc is not None, "Invalid response (failed request?)."
        state = vpc.get('State')
        assert state in ('pending', 'available'), f"Unexpected VPC state {state}."
        vpc_id = vpc.get('VpcId')

        vpc_obj = self.ec2_resource.Vpc(vpc_id)
        vpc_obj.create_tags(Tags=[self.vpc_tag])

        for igw_obj in vpc_obj.internet_gateways.all():
            igw_obj.create_tags(Tags=[self.get_tag('igw')])

        for subnet_obj in vpc_obj.subnets.all():
            subnet_obj.create_tags(Tags=[self.get_tag('subnet')])

        for rt_object in vpc_obj.route_tables.all():
            rt_object.create_tags(Tags=[self.get_tag('rt')])

        # 1 default security group is created and associated with default vpc
        security_group = next(iter(vpc_obj.security_groups.all()))

        data = self.ec2_client.authorize_security_group_ingress(
            GroupId=security_group.id,
            IpPermissions=[
                {'IpProtocol': 'tcp',
                 'FromPort': 22,
                 'ToPort': 22,
                 'IpRanges': [{'CidrIp': your_ip}]
                 },
            ])
        LOGGER.info('Ingress Successfully Set %s', data)

        security_group.create_tags(Tags=[self.get_tag('sg')])

        return vpc, vpc_id, security_group.id

    def setup(self, your_ip, keyout_file, instance_type):
        """Ensure AWS account is setup for upt, create a launch template and everything it needs."""
        # During setup, we check for VPC using vpc_filter. If UPT vpc already exists, we assume that other objects
        # exist too. If it doesn't we create everything.
        response = self.ec2_client.describe_vpcs(Filters=self.vpc_filter)
        if response.get('Vpcs'):
            vpc = response.get('Vpcs')[0]
            vpc_id = vpc.get('VpcId')
            vpc_obj = self.ec2_resource.Vpc(vpc_id)

            security_group = next(iter(vpc_obj.security_groups.all()))
            security_group_id = security_group.id
        else:
            vpc, vpc_id, security_group_id = self.create_default_vpc_with_tags(your_ip)

        # Get subnet id.
        response = self.ec2_client.describe_subnets(Filters=[{'Name': 'vpc-id', 'Values': [vpc_id]}])
        subnet_id = response.get('Subnets', [{}])[0].get('SubnetId', '')

        if not self.does_default_keypair_exist():
            assert keyout_file is not None, "You must specify a path where to create a keyfile, because you don't " \
                                            "have the default key in your account."
            self.create_keypair(keyout_file)

        # Create a launch template.
        self.create_template(instance_type, security_group_id, subnet_id)

    def create_keypair(self, keyout_file):
        """Create UPT default keypair in keyout_file location."""
        LOGGER.info('Creating UPT keypair...')

        response = self.ec2_client.create_key_pair(KeyName=UPT_DEFAULT_KEYNAME)
        key_material = response.get('KeyMaterial')
        with open(keyout_file, 'w') as fhandle:
            fhandle.write(key_material)

        return key_material

    def does_default_keypair_exist(self):
        """Return True if UPT default keypair exists, otherwise False."""
        LOGGER.info('Checking for UPT keypair...')
        return any(k.name == UPT_DEFAULT_KEYNAME
                   for k in self.ec2_resource.key_pairs.all())

    @staticmethod
    def launch_template_data(instance_type, security_group_id, subnet_id):
        """Prepare launch template data."""
        return {
            'EbsOptimized': False,
            'ImageId': FEDORA_AMI_ID,
            'InstanceType': instance_type,
            'KeyName': UPT_DEFAULT_KEYNAME,
            'NetworkInterfaces': [
                {
                    'Groups': [security_group_id],
                    'DeviceIndex': 0,
                    'SubnetId': subnet_id
                }
            ],
            'UserData': str(base64.b64encode(AWS_UPT_USER_DATA).decode('utf-8')),
        }

    @staticmethod
    def get_next_token():
        """Get client token."""
        return f'{datetime.utcnow().isoformat()}Z'

    def create_template(self, instance_type, security_group_id, subnet_id):
        """Create a launch template for UPT."""
        LOGGER.info('Creating launch template...')

        return self.ec2_client.create_launch_template(ClientToken=self.get_next_token(),
                                                      LaunchTemplateName=LAUNCH_TEMPLATE_NAME,
                                                      VersionDescription='0.1',
                                                      LaunchTemplateData=self.launch_template_data(instance_type,
                                                                                                   security_group_id,
                                                                                                   subnet_id))
